/*
 * Copyright (c) 2014, TrueNull.se
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification, are permitted provided that the following conditions are met:
 *
 * 1. Redistributions of source code must retain the above copyright notice, this list of conditions and the following disclaimer.
 *
 * 2. Redistributions in binary form must reproduce the above copyright notice, this list of conditions and the following disclaimer in the documentation and/or other materials provided with the distribution.
 *
 * 3. Neither the name of TrueNull.se nor the names of its contributors may be used to endorse or promote products derived from this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT HOLDER OR CONTRIBUTORS BE LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package se.truenull.school.conversion;

import android.content.res.Resources;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.app.ActionBarActivity;
import android.view.LayoutInflater;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.EditText;
import android.widget.Spinner;
import android.widget.TextView;




public class ConverterActivity extends ActionBarActivity {
    /**
     * 1 kg = this many lb
     */
    private static final Double conversion_factor_weight = 2.204622621849;
    /**
     * 1 kmph = this many mph
     */
    private static final Double conversion_factor_speed = 0.6213711922373;

    private Double current_value;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_converter);

        if (savedInstanceState == null) {
            getSupportFragmentManager().beginTransaction()
                    .add(R.id.container, new PlaceholderFragment())
                    .commit();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {

        // Inflate the menu; this adds items to the action bar if it is present.
        getMenuInflater().inflate(R.menu.converter, menu);
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        // Handle action bar item clicks here. The action bar will
        // automatically handle clicks on the Home/Up button, so long
        // as you specify a parent activity in AndroidManifest.xml.
        int id = item.getItemId();
        if (id == R.id.action_settings) {
            return true;
        }
        return super.onOptionsItemSelected(item);
    }

    public void onIncrementClick(View view) {
        setChangedValue(view);
        Double source = getCurrentValue();
        Double conversion_factor;

        String unit;
        unit = getConversionUnit();

        // Get target
        TextView target = (TextView) findViewById(R.id.conversion_target);

        conversion_factor = getConversionFactor();
        unit = getConversionUnit();

        target.setText(String.format("%.2f", (source * conversion_factor)) + unit);
    }

    private void setChangedValue(View view) {
        switch(view.getId())
        {
            case R.id.conversion_button_zero:
                incCurrentValue(0.0);
                break;
            case R.id.conversion_button_one:
                incCurrentValue(1.0);
                break;
            case R.id.conversion_button_ten:
                incCurrentValue(10.0);
                break;
            case R.id.conversion_button_fifty:
                incCurrentValue(50.0);
                break;
        }
    }

    private Double getCurrentValue() {
        String val;
        final TextView t = (TextView) findViewById(R.id.conversion_source);

        val = (t == null) ? "" : t.getText().toString();
        Double source = Double.parseDouble(val);
        return source;
    }

    private void incCurrentValue(Double d)
    {
        final TextView t = (TextView) findViewById(R.id.conversion_source);
        if(t != null)
        {
            Double source = getCurrentValue();

            String new_val =  (d==0)? "0": String.format("%.0f", source+d);

            t.setText(new_val);
        }
    }

    private String getConversionUnit() {
        // Get conversion type spinner
        Spinner spinner = (Spinner) findViewById(R.id.conversion_type);
        String v = spinner.getSelectedItem().toString();

        Resources res = getResources();
        String[] str = res.getStringArray(R.array.conversion_targets);

        Integer i = 0;
        for(; i < str.length; i++)
        {
            if(str[i].equals(v))
            {
                break;
            }
        }

        String unit = " ";

        switch (i)
        {
            case 0:
                unit += "km/h";
                break;
            case 1:
                unit += "mph";
                break;
            case 2:

                unit += "kg";
                break;
            case 3:
                unit += "lb";
                break;
        }

        return unit;
    }

    /**
     * A placeholder fragment containing a simple view.
     */
    public static class PlaceholderFragment extends Fragment {

        public PlaceholderFragment() {
        }

        @Override
        public View onCreateView(LayoutInflater inflater, ViewGroup container,
                                 Bundle savedInstanceState) {
            View rootView = inflater.inflate(R.layout.fragment_converter, container, false);
            return rootView;
        }
    }

    private double getConversionFactor()
    {
        // Get conversion type spinner
        Spinner spinner = (Spinner) findViewById(R.id.conversion_type);
        String v = spinner.getSelectedItem().toString();
        Resources res = getResources();
        String[] str = res.getStringArray(R.array.conversion_targets);
        Integer i = 0;
        for(; i < str.length; i++)
        {
            if(str[i].equals(v))
            {
                break;
            }
        }

        Double conversion_factor = 0.0;

        switch (i)
        {
            case 0:
                // Convert factor to 1 mph per x km/h
                conversion_factor = 1 / conversion_factor_speed;
                break;
            case 1:
                conversion_factor = conversion_factor_speed;
                break;
            case 2:
                // convert factor to 1 pound per x kg
                conversion_factor = 1 / conversion_factor_weight;
                break;
            case 3:
                conversion_factor = conversion_factor_weight;
                break;
        }

        return conversion_factor;
    }
}
